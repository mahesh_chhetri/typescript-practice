# Typescript practice 
This is typescript practice project


**Run applicaiton**
1. `npm install`
1. `npm run dev` - for development








### Configuring the typescript config file
`tsc --init` -  this will generates the default typescript configuration file

Now, enable the following variables 

1. "outDir": "./dist" - Here we are setting up the destination destination for outpu files
1. "rootDir": "./src" - Here i have declared the source folder where form compiler gets the source
1. "moduleResolution": "node"   


### Setting up with expressJs
1. `npm install express -S`
1. `npm install -D typescript ts-node nodemon @types/node @types/express`

Now, add the following scripts to `package.json`
```javascript 
 "start": "node dist/app.ts",
  "dev": "nodemon src/app.ts",
  "build": "tsc -p ."
```

