import express, { Application, Request, Response, NextFunction } from 'express';
import "./interface";


const app:Application = express();

app.get('/', (req:Request,res:Response) => {
  res.send("Hello world");
});

app.listen(4000, () => {
  console.log("SERVER IS RUNNING");
});