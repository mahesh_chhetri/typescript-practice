// Basic interface and function
var myFunc = (myObj: {name:string,phone: number,email: string}) => {
    return `My name is ${myObj.name} | Phone: ${myObj.phone} | email is ${myObj.email}`;
  }
  
  var params = {
    name: "Mahesh",
    phone: 5654654656,
    email: "mahesh@drivio.com"
  };
  
myFunc(params);