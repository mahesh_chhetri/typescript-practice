interface myObject {
  name: string;
  phone: number,
  ranks: number[],
  email?: string, // optional
  readonly pass: string // readonly - can't be reassign the value
}

function printLabel(myParam: myObject) {
  // myParam.pass = "+@34"+myParam.pass; 
  console.log(myParam.name, myParam.phone, myParam.ranks);
}

let values = {
  name: 'Mahesh',
  phone: 45646546,
  ranks: [2, 3, 5],
  pass: "45656"
};
printLabel(values);